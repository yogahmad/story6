from django.urls import path, include
from . import views
# from pembayaran.views import <namamethod>

urlpatterns = [
    path('', views.index),
    path('tambah/', views.tambah),
    path('hapus/', views.hapus),
]